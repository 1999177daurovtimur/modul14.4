﻿#include <iostream>
#include <string>

int main()
{
    
        std:: string str = "Project";

        std:: cout << str << '\n'; // Вывод строки
        std:: cout << str.length() << '\n'; // Вывод длины строки
        std:: cout << str.front() << '\n'; // Вывод 1го символа
        std::cout << str.back() << '\n'; // Вывод последнего символа из строки

        return 0;
   
}